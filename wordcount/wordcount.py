__author__ = 'ian'


class WordCount:
    def __init__(self, fileobject):
        self.lines = 0
        self.letters = 0
        self.words = 0
        for line in fileobject:
            self.lines += 1
            self.letters += len(line)
            line = line.split()
            self.words += len(line)

    def display_values(self):
        print 'words: ', self.words
        print 'letters: ', self.letters
        print 'lines: ', self.lines

    def save_to_file(self, filename='out.txt'):
            file = open(filename, 'w')
            file.write('words: ' + str(self.words) + '\n' + 'letters: ' + str(self.letters) + '\n' + 'lines: ' + str(self.lines))
            file.close()
