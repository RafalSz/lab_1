from unittest import TestCase
from wordcount.wordcount import WordCount
from mock import mock, MagicMock
from StringIO import StringIO
__author__ = 'ian'


class TestWordCount(TestCase):
    def test_object_should_have_correct_values(self):
        stringfile = StringIO()
        stringfile.write('There are \n'
                         'Four lines and \n'
                         '10 words and \n'
                         '51 letters')
        stringfile.seek(0, 0)
        wordcount = WordCount(stringfile)
        self.assertEqual(wordcount.letters, 51)
        self.assertEqual(wordcount.words, 10)
        self.assertEqual(wordcount.lines, 4)
        stringfile.close()

    @mock.patch('wordcount.wordcount.open', create=True)
    def test_save_to_file(self, mock_open):
        mock_open.return_value = MagicMock(spec=file)
        stringfile = StringIO('5 words and 22 letters')
        #wordcount.save_to_file("filename")
        #mock.open.assert_called_with("filename")
        #self.assertEqual('words: 5\nletters: 22\nlines: 1', str(stringfile))

