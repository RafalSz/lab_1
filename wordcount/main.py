import optparse
import sys
import logging
from wordcount import WordCount

logging.basicConfig(filename='wordcount.log', level=logging.DEBUG)

if __name__ == "__main__":
    parser = optparse.OptionParser("usage: %prog filename")
    (options, args) = parser.parse_args()
    if len(args) == 1:
        try:
            fileobject = open(sys.argv[1])
            logging.debug('File ' + str(sys.argv[1]) + ' opened')
            wordcount = WordCount(fileobject)
            logging.debug('Object wordcount created')
            wordcount.display_values()
            wordcount.save_to_file()
            logging.info('Saved to file')
            fileobject.close()
        except IOError as err:
            print(err)
            logging.error('Cant create file because:' + str(err))
    else:
            parser.error("incorrect number of arguments")
